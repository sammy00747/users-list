import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, Image } from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";

const arr=[1,2,3,4,5];

const App = () => {
  const [ DATA, updData ] = useState([]);
  const [ pgNo, setPgNo ] = useState(1);

  useEffect(()=>{
    const startFunc = async() => {
      let tmp=[];
      await fetch(`https://randomuser.me/api/?page=1&results=10`).then(res=>res.json()).then(data=>{
        tmp = [...data.results];
      }).catch(err=>{alert("An error occured while getting the updated list.")});
      if(tmp.length) {
        updData(tmp);
        setPgNo(2);
      }
      else {
        setPgNo(-1);
        await AsyncStorage.getItem("usersList",(err,res)=>{
          if(err) alert(err.message);
          else updData(JSON.parse(res) || []);
          console.log(res);
        })
      }
    };
    startFunc();
    return () => {
      AsyncStorage.setItem("usersList",JSON.stringify(DATA)).then(()=>{}).catch(()=>{});
    }
  },[]);

  const addUsers = () => {
    if(pgNo==-1) return;
    fetch(`https://randomuser.me/api/?page=${pgNo}&results=10`).then(res=>res.json()).then(data=>{
      updData([...DATA,...data.results]);
      setPgNo(pgNo+1);
    }).catch(()=>{"An error occured while getting the next set of users. Please try later"});
  }

  const renderItem = ({item}) => {
    const { medium } = item.picture;
    const { title, first, last } = item.name;
    const date = (new Date(item.dob.date)).toDateString();
    return (
      <View key={item.login.username} style={{padding:10,borderWidth:1,borderStyle:"solid",borderColor:"black",borderRadius:8,margin:8,display:"flex",flexDirection:"row",justifyContent:"space-between"}}>
        <View>
          <Image style={styles.profileImg} source={{uri:medium}} />
        </View>
        <View style={{flexGrow:1,paddingLeft:20}}>
          <Text>{title+' '+first+' '+last}</Text>
          <Text>{item.email}</Text>
          <Text>{date}</Text>
          <Text>{item.phone}</Text>
          <Text>{item.login.username}</Text>
        </View>
      </View>
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      {DATA.length ? <FlatList
        data={DATA}
        keyExtractor={item=>item.login.string}
        renderItem={renderItem}
        onEndReachedThreshold={0.5}
        onEndReached={addUsers}
      /> : <Text style={{fontSize:30}}>No users available!</Text>}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  profileImg: {
    width: 72,
    height: 72
  },
  title: {
    fontSize: 32,
  },
});

export default App;